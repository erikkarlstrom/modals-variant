import {Component, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})

export class ModalComponent implements OnInit {

  @Output('clickedOutside')
  clickedOutSide = new EventEmitter<boolean>();

  constructor(private el: ElementRef) { }
  ngOnInit() {
    // we added this so that when the backdrop is clicked the modal is closed.
    this.el.nativeElement.addEventListener('click', ()=> {
      if (!this.el.nativeElement.querySelector('.mmodal-body').contains(event.target)) {
        this.clickedOutSide.emit(true);
      }
    })
  }
}
