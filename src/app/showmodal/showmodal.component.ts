import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showmodal',
  templateUrl: './showmodal.component.html',
  styleUrls: ['./showmodal.component.css']
})
export class ShowmodalComponent implements OnInit {

  constructor() { }

  showModalToggle = false;

  saved = "not saved";

  ngOnInit(): void {
  }

  showDialog(){
    this.showModalToggle = true;
    this.saved = "not saved";
  }
  closeDialog() {
    // do we save?
    this.showModalToggle = false;
    this.saved = "saved!";
  }
}
